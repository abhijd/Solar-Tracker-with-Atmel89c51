# Solar Tracker with 89c51

A simple and inexpensive single-axis Solar Tracker implementation with 89c51 microcontroller and peripherals 

## Simulating

To simulate, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Proteus Design Suite


### How-to-simulate
 
 * Clone the repo in your local directory
 * Start Proteus Design Suite
 * In Proteus, open either "solar_project_finalised.DSN"  
 * When the design opens, double-click on the 89c51 microcontroller that opens the "edit component" pop-up
 * From the edit component, add the location of the solar_tracker_MUX_with_clock.hex file (located in the root of the project folder) in the program file field and click OK.
 * Press the play button from the bottom left menu. Voila!!

## Changing the program code

  * To change the program code, you may use any text editor to change the assembly code in thr file "solar_tracker_MUX_with_clock.asm" 
  * Use any assembly to hex converter program to convert the assembly code to hex format. (you may download one from www.microchip.com if you do not want to ask google)
  * Once changed and recompiled into hex file, load it again in the microcontroller as instructed in "How-to-simulate" section and run

## Hardware Implementation 

  * You must select the model of motor and its controller according to the frame and weight of installation. 
  * A simple voltage regulator circuit should be used to power the microcontroller
  * Before printing the circuit in board, experimentally make sure the hardware implementation work because the real world is not always similar to simulation world. We had to do several tweaks to make it work in the real world.  
  * The solar_tracker_MUX_with_clock.hex file must me burned (not literally) in the Atmega8 microcontroller using some universal programmer that supports it.
  

 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 

